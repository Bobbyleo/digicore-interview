package com.digicore.interview.validation;

import com.digicore.interview.exceptions.BadRequestException;
import com.digicore.interview.models.requests.AccountRequest;
import com.digicore.interview.models.requests.CreateAccountRequest;
import com.digicore.interview.models.requests.DepositRequest;
import com.digicore.interview.models.requests.WithdrawalRequest;

public class RequestValidator {

    public static void validateCreateAccountRequest(CreateAccountRequest request) throws BadRequestException {

        if(request.getAccountName() == null || request.getAccountName().isEmpty()){
            throw new BadRequestException(400, false, "Account name is required");
        }

        if(request.getAccountPassword() == null || request.getAccountPassword().isEmpty()){
            throw new BadRequestException(400, false, "Account password is required");
        }
    }

    public static void validateAccountRequest(AccountRequest request) throws BadRequestException {

        if(request.getAccountNumber() == null || request.getAccountNumber().isEmpty()){
            throw new BadRequestException(400, false, "Account number is required");
        }

        if(request.getAccountPassword() == null || request.getAccountPassword().isEmpty()){
            throw new BadRequestException(400, false, "Account password is required");
        }
    }

    public static void validateDepositRequest(DepositRequest request) throws BadRequestException {

        if(request.getAccountNumber() == null || request.getAccountNumber().isEmpty()){
            throw new BadRequestException(400, false, "Account number is required");
        }

        if(request.getAmount() == null ){
            throw new BadRequestException(400, false, "Amount is required");
        }else {
            if(request.getAmount() < 1){
                throw new BadRequestException(400, false, "Amount must be greater than 1");
            }

            if(request.getAmount() > 1000000){
                throw new BadRequestException(400, false, "Amount must be lesser than #1,000,000");
            }
        }
    }

    public static void validateWithdrawalRequest(WithdrawalRequest request) throws BadRequestException {

        if(request.getAccountNumber() == null || request.getAccountNumber().isEmpty()){
            throw new BadRequestException(400, false, "Account number is required");
        }

        if(request.getAccountPassword() == null || request.getAccountPassword().isEmpty()){
            throw new BadRequestException(400, false, "Account password is required");
        }

        if(request.getWithdrawnAmount() == null ){
            throw new BadRequestException(400, false, "Amount is required");
        }else {
            if(request.getWithdrawnAmount() < 1){
                throw new BadRequestException(400, false, "Amount must be greater than 1");
            }
        }
    }






}

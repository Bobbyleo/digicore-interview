package com.digicore.interview.services;

import com.digicore.interview.exceptions.BadRequestException;
import com.digicore.interview.models.AccountInfo;
import com.digicore.interview.models.AccountStatement;
import com.digicore.interview.models.requests.AccountRequest;
import com.digicore.interview.models.requests.CreateAccountRequest;
import com.digicore.interview.models.requests.DepositRequest;
import com.digicore.interview.models.requests.WithdrawalRequest;

import java.util.List;

public interface AccountService {

    void createAccount(CreateAccountRequest request) throws BadRequestException;
    void deposit(DepositRequest request) throws BadRequestException;
    void withdraw(WithdrawalRequest request) throws BadRequestException;
    List<AccountStatement> accountStatement(AccountRequest request) throws BadRequestException;
    AccountInfo getAccountInfo(AccountRequest request) throws BadRequestException;

}

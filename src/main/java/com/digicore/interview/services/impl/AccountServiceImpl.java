package com.digicore.interview.services.impl;

import com.digicore.interview.exceptions.BadRequestException;
import com.digicore.interview.models.Account;
import com.digicore.interview.models.AccountInfo;
import com.digicore.interview.models.AccountStatement;
import com.digicore.interview.models.requests.AccountRequest;
import com.digicore.interview.models.requests.CreateAccountRequest;
import com.digicore.interview.models.requests.DepositRequest;
import com.digicore.interview.models.requests.WithdrawalRequest;
import com.digicore.interview.repository.AccountRepository;
import com.digicore.interview.services.AccountService;
import com.digicore.interview.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void createAccount(CreateAccountRequest request) throws BadRequestException {

        //Check DB to see if account already name exists, send 400 error if it does
        if(accountRepository.accountNameExists(request.getAccountName()))
            throw new BadRequestException(400, false, "Account Name Already Exists");

        //Encode password using Bcrypt
        request.setAccountPassword(passwordEncoder.encode(request.getAccountPassword()));

        String generatedAccountNumber;

        //Generate a new account number that is UNIQUE
        do{
            generatedAccountNumber = AppUtils.generateAccountNumber();
        }while (accountRepository.accountNumberExists(generatedAccountNumber));

        accountRepository.createAccount(request, generatedAccountNumber);
    }

    @Override
    public void deposit(DepositRequest request) throws BadRequestException {

        //Get account from DB
        Account account = accountRepository.getAccountByAccountNumber(request.getAccountNumber());

        //Check DB to see if Account number exists, send 400 error if it does
        if (account == null) throw new BadRequestException(400, false, "Account Number Does Not Exist");

        accountRepository.deposit(request.getAccountNumber(), request.getAmount());
    }

    @Override
    public void withdraw(WithdrawalRequest request) throws BadRequestException {

        //Get account from DB
        Account account = getAccountAndValidatePassword(request.getAccountNumber(), request.getAccountPassword());

        //Get account balance from account
        double balance = accountRepository.getAccountBalance(request.getAccountNumber());

        //make sure that more than or at leasr 500 will be left in account after debit
        if(!(balance >= (request.getWithdrawnAmount() + 500.0)))
            throw new BadRequestException(400, false, "Insufficient Funds");

        accountRepository.withdraw(account.getAccountNumber(), request.getWithdrawnAmount());
    }

    @Override
    public AccountInfo getAccountInfo(AccountRequest request) throws BadRequestException {
        //Get Account
        Account account = getAccountAndValidatePassword(request.getAccountNumber(), request.getAccountPassword());

        //Convert to AccountInfo and return
        return new AccountInfo(account);
    }


    @Override
    public List<AccountStatement> accountStatement(AccountRequest request) throws BadRequestException {

        //Get Account
        Account account = getAccountAndValidatePassword(request.getAccountNumber(), request.getAccountPassword());
        //Retrieve accountStatement
        List<AccountStatement> accountStatements = accountRepository.getAccountStatement(account.getAccountNumber());

        //Return empty list if accountStatements is null, otherwise return accountStatements
        return  accountStatements != null? accountStatements : new ArrayList<>();
    }


    public Account getAccountAndValidatePassword(String accountNumber, String password) throws BadRequestException {

        //Get account from Repo
        Account account = accountRepository.getAccountByAccountNumber(accountNumber);


        if (account == null) throw new BadRequestException(400, false, "Account Number Does Not Exist");

        //Check that correct password was provided
        if(!passwordEncoder.matches(password, account.getAccountPassword()))
            throw new BadRequestException(401, false, "Incorrect Password!!!");

        return account;
    }


}

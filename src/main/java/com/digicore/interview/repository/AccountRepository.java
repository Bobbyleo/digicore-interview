package com.digicore.interview.repository;

import com.digicore.interview.database.DigicoreDB;
import com.digicore.interview.models.Account;
import com.digicore.interview.models.AccountStatement;
import com.digicore.interview.models.TransactionType;
import com.digicore.interview.models.requests.CreateAccountRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class AccountRepository {

    public void createAccount(CreateAccountRequest request, String accountNumber){

        //Create a new account and insert in DB

        Account newAccount = Account.builder()
                .accountName(request.getAccountName())
                .accountNumber(accountNumber)
                .balance(0.0)
                .accountPassword(request.getAccountPassword())
                .build();
        DigicoreDB.accounts.put(accountNumber, newAccount);

        //Insert empty list to account statement
        DigicoreDB.accountStatements.put(accountNumber, new ArrayList<>());
    }

    public boolean accountNumberExists(String accountNumber){

        //Check if account number exists in db
        return DigicoreDB.accounts.containsKey(accountNumber);
    }

    public boolean accountNameExists(String accountName){

        //Checks the DB to see if account name already exists

        return DigicoreDB.accounts.values()
                .stream()
                .map(Account::getAccountName)
                .anyMatch(name -> name.equals(accountName));
    }

    public List<AccountStatement> getAccountStatement(String accountNumber){
        //Gets account statement from db
        return DigicoreDB.accountStatements.get(accountNumber);
    }

    public Account getAccountByAccountNumber(String accountNumber){

        //Gets account from db
        return DigicoreDB.accounts.get(accountNumber);
    }

    private void updateAccountInDb(Account account){
        DigicoreDB.accounts.put(account.getAccountNumber(), account);
    }

    private void updateAccountStatementInDB(List<AccountStatement> statements, String accountNumber){

        DigicoreDB.accountStatements.put(accountNumber, statements);
    }

    public Double getAccountBalance(String accountNumber){

        return DigicoreDB.accounts.get(accountNumber).getBalance();
    }
    public List<AccountStatement> getAccountStatements(String accountNumber){

        return  DigicoreDB.accountStatements.get(accountNumber);
    }


    public void deposit(String accountNumber, double amount){

        //Retrieve account by accountNumber
        Account account = getAccountByAccountNumber(accountNumber);

        //Add and update the account balance in DB
        account.setBalance(account.getBalance() + amount);
        updateAccountInDb(account);

        //Retrieve accountStatement
        List<AccountStatement> accountStatements = getAccountStatements(accountNumber);

        //Add and update the account balance in DB
        accountStatements.add(
                AccountStatement.builder()
                .transactionDate(new Date())
                .transactionType(TransactionType.Deposit.name())
                .narration("")
                .amount(amount)
                .accountBalance(account.getBalance())
                .build()
        );

        updateAccountStatementInDB(accountStatements, accountNumber);
    }

    public void withdraw(String accountNumber, double amount){

        //Retrieve account by accountNumber
        Account account = getAccountByAccountNumber(accountNumber);

        //Deduct and update the account balance in DB
        account.setBalance(account.getBalance() - amount);
        updateAccountInDb(account);

        //Retrieve accountStatement
        List<AccountStatement> accountStatements = getAccountStatements(accountNumber);

        //Add and update the account balance in DB
        accountStatements.add(
                AccountStatement.builder()
                        .transactionDate(new Date())
                        .transactionType(TransactionType.Withdrawal.name())
                        .narration("")
                        .amount(amount)
                        .accountBalance(account.getBalance())
                        .build()
        );

        updateAccountStatementInDB(accountStatements, accountNumber);
    }
}

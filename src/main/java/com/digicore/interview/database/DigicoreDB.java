package com.digicore.interview.database;

import com.digicore.interview.models.Account;
import com.digicore.interview.models.AccountStatement;

import java.util.HashMap;
import java.util.List;

public class DigicoreDB {

    public static HashMap<String, Account> accounts = new HashMap<>();
    public static HashMap<String, List<AccountStatement>> accountStatements = new HashMap<>();
}

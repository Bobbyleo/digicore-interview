package com.digicore.interview.controllers;



import com.digicore.interview.models.AccountInfo;
import com.digicore.interview.models.AccountStatement;
import com.digicore.interview.models.requests.AccountRequest;
import com.digicore.interview.models.requests.CreateAccountRequest;
import com.digicore.interview.models.requests.DepositRequest;
import com.digicore.interview.models.requests.WithdrawalRequest;
import com.digicore.interview.models.response.APIDataResponse;
import com.digicore.interview.models.response.APIResponse;
import com.digicore.interview.services.AccountService;
import com.digicore.interview.validation.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
public class AccountController {

    @Autowired
    AccountService accountService;


    @PostMapping("/create_account")
    ResponseEntity<APIResponse> register(@RequestBody CreateAccountRequest request) throws Exception {

        //Validate that the request body has the required params
        RequestValidator.validateCreateAccountRequest(request);
        accountService.createAccount(request);
        return ResponseEntity.ok(new APIResponse(200, true,"Account created Successfully"));
    }

    @PostMapping("/account_info")
    ResponseEntity<APIDataResponse<AccountInfo>> getAccount(@RequestBody AccountRequest request) throws Exception {

        //Validate that the request body has the required params
        RequestValidator.validateAccountRequest(request);
        AccountInfo accountInfo = accountService.getAccountInfo(request);

        return ResponseEntity.ok(new APIDataResponse<>(200, true,
                "Account retrieved successfully", accountInfo));
    }

    @PostMapping("/account_statement")
    ResponseEntity<List<AccountStatement>> accountStatement(@RequestBody AccountRequest request) throws Exception {

        //Validate that the request body has the required params
        RequestValidator.validateAccountRequest(request);
        List<AccountStatement> accountStatement = accountService.accountStatement(request);
        return ResponseEntity.ok(accountStatement);
    }

    @PostMapping("/deposit")
    ResponseEntity<APIResponse> deposit(@RequestBody DepositRequest request) throws Exception {


        //Validate that the request body has the required params
        RequestValidator.validateDepositRequest(request);
        accountService.deposit(request);
        return ResponseEntity.ok(new APIResponse(200, true,"Amount deposited Successfully"));
    }

    @PostMapping("/withdrawal")
    ResponseEntity<APIResponse> withdraw(@RequestBody WithdrawalRequest request) throws Exception {


        //Validate that the request body has the required params
        RequestValidator.validateWithdrawalRequest(request);
        accountService.withdraw(request);
        return ResponseEntity.ok(new APIResponse(200, true,"Amount withdrawn Successfully"));
    }

}

package com.digicore.interview.exceptions;

import com.digicore.interview.models.response.APIResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<APIResponse> handleOrderBadRequestException(BadRequestException ex) {
        APIResponse error = new APIResponse( ex.getResponseCode(), false, ex.getMessage() );
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<APIResponse> nullPointerExceptionHandler(Exception ex) {
        APIResponse error = new APIResponse(400, false, ex.getMessage());
        ex.printStackTrace();
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<APIResponse> exceptionHandler(Exception ex) {
        APIResponse error = new APIResponse(400, false, ex.getMessage());
        System.out.println(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}

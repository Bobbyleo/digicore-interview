package com.digicore.interview.exceptions;

import lombok.Builder;
import lombok.Data;

import java.io.IOException;

@Data
@Builder
public class BadRequestException extends IOException {

    int responseCode;
    private boolean success;
    private String message;

    public BadRequestException() {
        super();
    }

    public BadRequestException(String message) {
        super(message);
        this.message = message;
    }

    public BadRequestException(int responseCode, boolean success, String message) {
        super(message);
        this.responseCode = responseCode;
        this.success = success;
        this.message = message;
    }
}

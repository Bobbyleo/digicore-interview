package com.digicore.interview.beanbasket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class BeanBasket {

    /*
     * PLEASE DO NOT DELETE, THIS BEANS ARE AUTO INJECTED AND ARE NOT CALLED DIRECTLY
     * SIGNED: BOBBY UTULU
     * */

    @Bean
    public PasswordEncoder getPassword(){
        //10 is the strength of the password encoder
        return new BCryptPasswordEncoder(10);
    }

}


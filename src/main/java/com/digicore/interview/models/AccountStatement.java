package com.digicore.interview.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountStatement {

    Date transactionDate;
    String transactionType;
    String narration;
    Double amount;
    Double accountBalance;
}

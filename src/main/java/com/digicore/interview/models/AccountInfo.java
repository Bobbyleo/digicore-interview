package com.digicore.interview.models;

import lombok.Data;

@Data
public class AccountInfo {

    String accountName;
    String accountNumber;
    Double balance;

    public AccountInfo(Account account){
        this.accountName = account.accountName;
        this.accountNumber = account.accountNumber;
        this.balance = account.balance;
    }
}


package com.digicore.interview.models.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class APIDataResponse<T> {

    int responseCode;
    boolean success;
    String message;
    T account;
}

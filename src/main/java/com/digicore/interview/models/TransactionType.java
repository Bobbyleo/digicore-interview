package com.digicore.interview.models;

public enum TransactionType {
     Deposit, Withdrawal
}

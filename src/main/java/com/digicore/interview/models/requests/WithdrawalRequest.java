package com.digicore.interview.models.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WithdrawalRequest {

    String accountNumber;
    String accountPassword;
    Double withdrawnAmount;
}

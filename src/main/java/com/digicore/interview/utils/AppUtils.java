package com.digicore.interview.utils;

import java.util.Random;

public class AppUtils {

    public static String generateAccountNumber() {
        //Generate account number

        StringBuilder buffer = new StringBuilder(10);

        String numbers = "0123456789";
        Random random = new Random();

        for(int i = 0; i< 10; i++){
            //for each iteration of i select a number from 0-9 and append to StringBuilder

            char x = numbers.charAt(random.nextInt(numbers.length()));
            buffer.append(x);
        }

        return buffer.toString();
    }
}

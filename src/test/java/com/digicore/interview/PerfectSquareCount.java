package com.digicore.interview;

public class PerfectSquareCount {

    public static void main(String[] args) {

        //pass n and multiple testcases to the checkGrids method
        checkGridsPerfectSquareCount(3, new int[]{1,1}, new int[]{3, 3}, new int[]{4, 5});

    }

    public static void checkGridsPerfectSquareCount(int n, int[]... pairs){
        //Varargs are used so that multiple test cases can be passed as argument

        for(int i = 0; i < n; i++ ){

            //For each test case (block)
            //Pass the length and width to the method "getNumberOfSquaresInBlock"
            int[] block = pairs[i];
            System.out.println(getNumberOfSquaresInBlock(block[0], block[1]));
        }
    }

    public static int getNumberOfSquaresInBlock(int length, int width){
        //This method returns the number of perfect square IN the block

        //keep track of square grids with length more than to avoid counting the outermost square
        boolean isSquareBlock = (length == width) && ((length * width) != 1);

        //magic :D
        int x = ((length) * (length + 1) * ((3 * width) - (length -1))) / 6;

        //return no of perfect squares deducting the outermost square if the block is a square block
        return isSquareBlock? x - 1 : x;
    }
}

package com.digicore.interview;

import com.digicore.interview.utils.AppUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AppUtilsTests {

    @Test
    public void accountNumberLengthTest(){


        String accountNumber = AppUtils.generateAccountNumber();

        assertFalse(accountNumber.isEmpty());
        assertEquals(10, accountNumber.length());
    }
}
